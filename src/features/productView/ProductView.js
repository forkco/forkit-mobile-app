import React, { Component } from 'react';
import { StyleSheet, View, Text, Dimensions } from 'react-native';

import { Title, Divider, Caption, TextInput, Subtitle } from '@shoutem/ui';

import { Button, Icon } from 'react-native-elements';

import NumericInput from 'react-native-numeric-input';

import { connect } from 'react-redux';

import HeaderImageScrollView from 'react-native-image-header-scroll-view';

import * as Animatable from 'react-native-animatable';

class ProductView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      quantity: 1
    };
  }

  render() {
    var i;
    let product = this.props.navigation.getParam('product');
    var listIngredients = product.ingredients
      .map(item => {
        return item.name;
      })
      .join(', ');
    return (
      <View style={{ flex: 1 }}>
        <HeaderImageScrollView
          maxHeight={Dimensions.get('window').height / 3}
          minHeight={55}
          headerImage={{
            uri: 'https://forkittest.gigalixirapp.com' + product.image
          }}
          renderFixedForeground={() => (
            <View style={{}}>
              <Animatable.View
                style={{ position: 'absolute' }}
                ref={backArrow => {
                  this.backArrow = backArrow;
                }}
              >
                <Icon
                  name="arrow-back"
                  color='white'
                  containerStyle={{ top: 10, left: 8, zIndex: 1 }}
                  size={35}
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}
                  underlayColor="transparent"
                />
              </Animatable.View>
            </View>
          )}
        >
          <View style={styles.infoContainer}>
            <Title style={styles.title}> {product.name} </Title>
            <Divider styleName="section-header">
              <Caption>Descrição</Caption>
            </Divider>

            <Subtitle style={styles.description}>
              {product.description}
            </Subtitle>
            <Text style={styles.ingredients}>
              <Text style={{ fontWeight: 'bold' }}> Ingredientes: </Text>
              {listIngredients}
            </Text>

            <View style={styles.selectionContainer}>
              <NumericInput
                style={{ alignSelf: 'flex-start' }}
                value={product.quantity}
                onChange={quantity => this.setState({ quantity: quantity })}
                totalWidth={150}
                minValue={1}
                maxValue={99}
                totalHeight={35}
                iconSize={15}
                step={1}
                initValue={this.state.quantity}
                valueType="integer"
                rounded="true"
                textColor="black"
                iconStyle={{ color: 'white' }}
                rightButtonBackgroundColor="grey"
                leftButtonBackgroundColor="grey"
              />
              <Text style={styles.price}> {product.price * 0.01 + '€'}</Text>
            </View>

            <Divider styleName="section-header">
              <Caption>Instruções Especiais</Caption>
            </Divider>
            <TextInput
              style={styles.noteInput}
              placeholder={'Indique as suas preferências à cozinha'}
              onChangeText={text => this.setState({ note: text })}
            />
              <Button
                containerStyle={{
                  marginTop: 15,
                  marginBottom: 15,
                  marginLeft: 20,
                  marginRight: 20
                }}
                buttonStyle={{
                  backgroundColor: '#a83432',
                  width: '100%',
                //  height: 45,
                //  borderColor: 'transparent',
                //  borderWidth: 0,
                //  borderRadius: 5
                }}
                icon={{
                  name: 'shopping-cart',
                  size: 25,
                  color: 'white',
                }}
                title={
                  'Adicionar ao pedido: ' +
                  (product.price * this.state.quantity) / 100 +
                  ' €'
                }
                onPress={() => {
                  for (i = 0; i < this.state.quantity; i++) {
                    product.available ? this.props.addProduct(product) : {};
                    this.props.navigation.goBack();
                  }
                }}
              />
          </View>
        </HeaderImageScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    order: state.order.activeOrder
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addProduct: product => dispatch({ type: 'ADD_PRODUCT', product })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductView);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 2,
    marginTop: 10
  },
  selectionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    padding: 20
  },
  price: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#a83432'
  },
  title: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    marginBottom: 10,
    padding: 5,
    fontWeight: 'bold'
  },
  description: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    marginBottom: 10,
    padding: 10
  },
  noteInput: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    marginBottom: 10
  },
  ingredients: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    padding: 7,
    marginBottom: 10
  },
  reqButton: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'center'
  }
});
