import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Keyboard,
  TouchableOpacity
} from 'react-native';
import { SearchBar, ListItem, Card } from 'react-native-elements';
import { createFilter } from 'react-native-search-filter';
import { ImageScrollView } from '../../components/ImageScrollView.js';
import { connect } from 'react-redux';

const KEYS_TO_FILTERS = ['address.city', 'address.street'];
const MIN_HEIGHT = 55;

class SearchEstablishment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      showHeader: true,
      establishments: []
    };
  }

  componentDidMount() {
    return fetch('http://forkittest.gigalixirapp.com/api/restaurants')
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            establishments: responseJson.data
          },
          function() {}
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  renderScheduleInfo() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <Text style={{ paddingTop: 7, color: 'green', fontWeight: 'bold' }}>
          Aberto
        </Text>
      </View>
    );
  }

  renderDistanceInfo(address, distance) {
    return (
      <View style={{ flexDirection: 'row', paddingBottom: 5 }}>
        <Text style={styles.distance}> {distance} </Text>
        <Text style={{ paddingLeft: 5, paddingTop: 3 }}> {address} </Text>
      </View>
    );
  }

  renderSearchBar() {
    return (
      <SearchBar
        platform="android"
        cancelIcon={{
          onPress: () => {
            this.searchUpdated('');
            Keyboard.dismiss();
          }
        }}
        clearIcon={{ name: 'close' }}
        placeholder="Search Restaurant"
        onChangeText={term => {
          this.searchUpdated(term);
        }}
        onClear={() => {
          this.searchUpdated('');
        }}
      />
    );
  }

  renderCards(establishments) {
    return (
      <View>
        {establishments.map((l, i) => (
          <TouchableOpacity
            key={i}
            onPress={() => {
              this.props.setRestaurantId(l.id);
              this.props.navigation.navigate('ListCategories', {
                restaurantId: l.id
              });
            }}
          >
            <Card
              image={establishmentsImages[l.id-1]}
              imageProps={{
                borderTopLeftRadius: 15,
                borderTopRightRadius: 15
              }}
              containerStyle={{ borderRadius: 15, elevation: 2 }}
            >
              <View style={(styles.row, { textAlign: 'center' })}>
                <Text style={styles.title}> {l.address.city}</Text>
                <Text style={styles.subtitle}>Aberto</Text>
              </View>
              <Text>{l.address.street}</Text>
            </Card>
          </TouchableOpacity>
        ))}
      </View>
    );
  }

  renderItems(establishments) {
    return (
      <View>
        {establishments.map((l, i) => (
          <View key={i}>
            <ListItem
              key={i}
              title={
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    style={{
                      fontSize: 18,
                      marginRight: 10
                    }}
                  >
                    {l.address.city}
                  </Text>
                </View>
              }
              subtitle={
                <Text>{String(l.address.street).replace(/,.*/, '')}</Text>
              }
              rightElement={
                <Text
                  style={{
                    color: 'green',
                    fontWeight: 'bold'
                  }}
                >
                  Aberto
                </Text>
              }
              chevron={false}
              onPress={() => {
                this.props.setRestaurantId(l.id);
                this.props.navigation.navigate('ListCategories', {
                  restaurantId: l.id
                });
              }}
              containerStyle={{
                elevation: 3
              }}
            />
            <View style={{ height: 12 }} />
          </View>
        ))}
      </View>
    );
  }

  render() {
    const filteredEstablishments = this.state.establishments.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );
    return (
      <ImageScrollView
        scrollContent={this.renderCards(filteredEstablishments)}
        headerImage={require('./images/header.jpg')}
        logoImage={require('./images/fokit-text.png')}
        //leftComponent={<Text>ForkIt</Text>}
        searchUpdated={this.searchUpdated.bind(this)}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    restaurantId: state.restid.restaurantId
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setRestaurantId: restaurantId =>
      dispatch({ type: 'SET_RESTAURANT_ID', restaurantId })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchEstablishment);

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 25
  },
  subtitle: {
    fontWeight: 'bold',
    lineHeight: 25,
    position: 'absolute',
    right: 0,
    color: '#57a834'
  },
  navTitleView: {
    height: MIN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    opacity: 0
  }
});

var establishmentsImages = [
  require('./images/restaurant1.jpg'),
  require('./images/restaurant2.jpg'),
  require('./images/restaurant3.jpg'),
  require('./images/restaurant4.jpg'),
  require('./images/restaurant5.jpg'),
  require('./images/restaurant6.jpg')
]

const establishments = [
  {
    location: 'Braga',
    address: 'Rua Nova de Santa Cruz, 14',
    distance: '214 M'
  },
  {
    location: 'Coimbra',
    address: 'Avenida Sá Da Bandeira, 114',
    distance: '20 Km'
  },
  {
    location: 'Figueira da Foz',
    address: 'Rua Bernardo Lopes, 45',
    distance: '34 Km'
  },
  {
    location: 'Leiria',
    address: 'Rua Monte de Aviz, 9',
    distance: '51 Km'
  },
  {
    location: 'Lisboa - Campo Grande',
    address: 'Rua Ernesto De Vasconcelos, 193',
    distance: '56 Km'
  },
  {
    location: 'Lisboa - Dom Luís',
    address: 'Praça Dom Luís I, 10',
    distance: '67 Km'
  },
  {
    location: 'Lisboa - Oriente',
    address: 'Rua Pimenta, 103',
    distance: '83 Km'
  },
  {
    location: 'Odivelas',
    address: 'Rua Pulido Valente, 7',
    distance: '96 Km'
  },
  {
    location: 'Lisboa - Duque de Avila',
    address: 'Av. Duque de Ávila, 47',
    distance: '102 Km'
  },
  {
    location: 'Almancil',
    address: 'Avenida do Algarve',
    distance: '120 Km'
  },
  {
    location: 'Vila Nova de Gaia - Arrábida Shopping',
    address: 'Praceta de Henrique Moreira, 244',
    distance: '154 Km'
  },
  {
    location: 'Porto',
    address: 'Estrada De Circunvalação, 7612',
    distance: '181 Km'
  },
  {
    location: 'Almada Forum',
    address: 'Estrada Do Caminho Municipal, 1011',
    distance: '194 Km'
  }
];
