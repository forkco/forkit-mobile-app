import React from 'react';
import { Card } from 'react-native-elements';
import { Text, StyleSheet } from 'react-native';

const CategoryCard = ({ index, image, text }) => {
  const { textStyle, imageStyle, containerStyle } = styles;

  return (
    <Card
      key={index}
      containerStyle={containerStyle}
      image={image}
      imageProps={imageStyle}
    >
      <Text style={textStyle}>{text}</Text>
    </Card>
  );
};

const endpoint = 'https://forkittest.gigalixirapp.com';
const styles = StyleSheet.create({
  textStyle: {
    fontWeight: '300',
    fontSize: 15
  },

  imageStyle: {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    opacity: 0.9
  },

  containerStyle: {
    borderRadius: 15
  }
});

export { CategoryCard };
