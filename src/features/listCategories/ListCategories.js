import React, { Component } from 'react';
import { View, TouchableHighlight } from 'react-native';
import { CategoryCard } from './components/CategoryCard';
import { ImageScrollView } from '../../components/ImageScrollView.js';
import { createFilter } from 'react-native-search-filter';

const KEYS_TO_FILTERS = ['name'];

class ListCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      searchTerm: ''
    };
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  componentDidMount() {
    const restaurantId = this.props.navigation.getParam('restaurantId');
    return fetch(
      'http://forkittest.gigalixirapp.com/api/restaurants/' +
        restaurantId +
        '/categories'
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState(
          {
            categories: responseJson.data
          },
          function() {}
        );
      })
      .catch(error => {
        console.error(error);
      });
  }

  renderCards(categories) {
    return (
      <View>
        {categories.map((u, i) => (
          <TouchableHighlight
            key={i}
            onPress={() =>
              this.props.navigation.navigate('ListProducts', {
                category: i,
                categories: this.state.categories
              })
            }
            underlayColor="white"
          >
            <CategoryCard index={i} text={u.name} image={categoryImages[u.id-1]} />
          </TouchableHighlight>
        ))}
      </View>
    );
  }

  _renderScrollView() {
    return (
      <View>
        {this.state.categories.map((u, i) => {
          <TouchableHighlight
            key={i}
            onPress={() =>
              this.props.navigation.navigate('ListProducts', {
                category: i,
                categories: this.state.categories
              })
            }
            underlayColor="white"
          >
            <CategoryCard index={i} text={u.name} image={u.image} />
          </TouchableHighlight>;
        })}
        <View style={{ marginVertical: 10 }} />
      </View>
    );
  }

  render() {
    const filteredCategories = this.state.categories.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );

    return (
      <ImageScrollView
        scrollContent={this.renderCards(filteredCategories)}
        headerImage={require('../../assets/images/flatlay.jpg')}
        leftComponent={{
          icon: 'arrow-back',
          color: 'white',
          size: 35,
          underlayColor: 'transparent',
          onPress: () => this.props.navigation.goBack()
        }}
        centerComponent={{
          text: 'Menu',
          style: { color: 'white', fontSize: 20, fontWeight: '400' }
        }}
        searchUpdated={this.searchUpdated.bind(this)}
      />
    );
  }
}

export default ListCategories;

var categoryImages = [
  require('./images/category1.jpg'),
  require('./images/category2.jpg'),
  require('./images/category3.jpg'),
  require('./images/category4.png')
]