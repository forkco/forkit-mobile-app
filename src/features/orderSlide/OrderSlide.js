import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Icon } from 'react-native-elements';
import SlidingUpPanel from 'rn-sliding-up-panel';
import OrderTabView from './components/OrderTabView.js';
import { View, Dimensions, Animated, Alert } from 'react-native';

const { height } = Dimensions.get('window');

class OrderSlide extends Component {
  static defaultProps = {
    draggableRange: {
      top: height * 0.9,
      bottom: height * 0.075
    }
  };

  _draggedValue = new Animated.Value(-height * 0.1);

  state = {
    slideUp: false,
    allowDragging: true,
    index: 0,
    routes: [
      { key: 'first', title: 'Pedido' },
      { key: 'second', title: 'Pagos' }
    ]
  };

  constructor(props) {
    super(props);
    styles.panelHeader.height = height * 0.04;
  }

  sendOrder() {
    let order = { type: 0, status: 1, requests: [], note: 'note11' };
    for (var i = 0; i < this.props.order.products.length; i++) {
      let prod = this.props.order.products[i];
      for (var u = 0; u < prod.units; u++) {
        order.requests.push({
          product_id: prod.id,
          note: 'note22',
          ingredients: []
        });
      }
    }
    fetch(
      'http://forkittest.gigalixirapp.com/api/restaurants/' +
        this.props.restaurantId +
        '/orders',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          order: order
        })
      }
    )
      .then(response => response.json())
      .then(responseJson => {
        responseJson.data.totalPrice = this.props.order.totalPrice
        fetch(
          'http://forkittest.gigalixirapp.com/api/restaurants/' +
            this.props.restaurantId +
            '/orders/' + responseJson.data.id + '/payments',
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              payment:{
                amount: responseJson.data.totalPrice,
                mbway_payment: {
                  merchant_id: 1
                }
              }
            })
          }
        )
          .then(() => {
              //Alert.alert(responseJson)
              //add order to state so we can show it to the user
              this.props.addPaidOrder(responseJson.data);
              //reset the orderslide data
              this.props.resetActiveOrder();
          })
          .catch(error => {
            Alert.alert(error);
          });
      })
      .catch(error => {
        console.alert(error);
      });
  }

  renderOrderSlide() {
    return (
      <SlidingUpPanel
        ref={c => (this._panel = c)}
        visible
        height={height * 0.87}
        startCollapsed
        //allowDragging={this.state.allowDragging}
        draggableRange={this.props.draggableRange}
        minimumDistanceThreshold={10}
        //onDrag={v => {this._draggedValue.setValue(v); this._draggedValue < 30 ? this.setState({ slideUp: false }) : {}}}
        onRequestClose={() =>
          this._panel.transitionTo({
            toValue: 10,
            duration: 400,
            onAnimationEnd: () => this.setState({ slideUp: false })
          })
        }
      >
        <View style={styles.sliderContainer}>
          {/* HEADER */}
          <View style={styles.panelHeader}>
            {this.state.slideUp ? (
              <Icon
                name="expand-more"
                onPress={() => {
                  this._panel.transitionTo({
                    toValue: 10,
                    duration: 400,
                    onAnimationEnd: () => this.setState({ slideUp: false })
                  });
                }}
              />
            ) : (
              <Icon
                name="expand-less"
                onPress={() => {
                  this._panel.transitionTo({
                    toValue: 1000,
                    duration: 400,
                    onAnimationEnd: () => this.setState({ slideUp: true })
                  });
                }}
              />
            )}
          </View>
          {/* HEADER END */}
          {/* BODY */}
          <View style={{ backgroundColor: 'black', flex: 1 }}>
            <OrderTabView sendOrder={this.sendOrder.bind(this)} />
          </View>
          {/* BODY END */}
        </View>
      </SlidingUpPanel>
    );
  }

  render() {
    //if(this.state.slideUp) this.setState({ slideUp: false })
    return (
      (this.props.order.totalPrice > 0 || this.props.orders.length > 0) &&
      this.renderOrderSlide()
    );
  }
}

const styles = {
  sliderContainer: {
    flex: 1,
    zIndex: 1,
    backgroundColor: 'transparent'
  },
  panelHeader: {
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomWidth: 0.5,
    backgroundColor: '#e6e6e6',
    overflow: 'hidden'
  }
};

function mapStateToProps(state) {
  return {
    order: state.order.activeOrder,
    orders: state.order.paidOrders,
    restaurantId: state.restid.restaurantId
  };
}

function mapDispatchToProps(dispatch) {
  return {
    removeProduct: product => dispatch({ type: 'REMOVE_PRODUCT', product }),
    addPaidOrder: order => dispatch({ type: 'ADD_PAID_ORDER', order }),
    resetActiveOrder: () => dispatch({ type: 'RESET_ACTIVE_ORDER' })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderSlide);
