import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements';
import { Text, View } from 'react-native';

class PaidOrders extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    var i = 1;
    return (
      <View style={{ flex: 1 }}>
        {this.props.orders.map((item, x) => {
          var status = '';
          switch (item.status) {
            case 0:
              status = 'Pendente';
              break;
            case 1:
              status = 'Em processamento';
              break;
            case 2:
              status = 'Terminado';
              break;
          }
          return (
            <View key={x}>
              <ListItem
                key={x}
                title={'Pedido ' + i++}
                titleStyle={{ fontWeight: 'bold' }}
                subtitle={status}
                rightElement={
                  <Text style={{}}>{item.totalPrice.toFixed(2)}€</Text>
                }
                bottomDivider={true}
                //onPress={() => Alert.alert('Show order')}
              />
            </View>
          );
        })}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    order: state.order.activeOrder,
    orders: state.order.paidOrders,
    restaurantId: state.restid.restaurantId
  };
}

export default connect(mapStateToProps)(PaidOrders);
