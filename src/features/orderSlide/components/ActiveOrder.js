import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ListItem } from 'react-native-elements';
import { View } from 'react-native';

class ActiveOrder extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.props.order.products.map((item, x) => (
          <View key={item.name}>
            <ListItem
              key={x}
              title={item.name}
              subtitle={`${(item.units * item.price * 0.01).toFixed(2)}€ (${
                item.units
              } x ${(item.price * 0.01).toFixed(2)}€)`}
              leftAvatar={{
                source: {
                  uri: 'https://forkittest.gigalixirapp.com' + item.image
                }
              }}
              rightIcon={{
                name: 'remove-circle',
                onPress: () => {
                  this.props.removeProduct(item);
                }
              }}
              containerStyle={{
                borderColor: 'gray',
                borderTopWidth: 0.5,
                borderBottomWidth: 0.5
              }}
            />
          </View>
        ))}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    order: state.order.activeOrder,
    orders: state.order.paidOrders,
    restaurantId: state.restid.restaurantId
  };
}

function mapDispatchToProps(dispatch) {
  return {
    removeProduct: product => dispatch({ type: 'REMOVE_PRODUCT', product }),
    resetActiveOrder: () => dispatch({ type: 'RESET_ACTIVE_ORDER' })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ActiveOrder);
