import React, { Component } from 'react';
import {
  Dimensions,
  ScrollView,
  Animated,
  Text,
  Image,
  TouchableHighlight
} from 'react-native';
import { Icon } from 'react-native-elements';
import { TabView, TabBar } from 'react-native-tab-view';
import ActiveOrder from './ActiveOrder.js';
import PaidOrders from './PaidOrders.js';
import { connect } from 'react-redux';
import { View } from 'react-native-animatable';
import Dialog, {
  DialogContent,
  DialogButton,
  DialogTitle,
  DialogFooter
} from 'react-native-popup-dialog';

class OrderTabView extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    showPaymentOptions: false,
    index: 0,
    routes: [
      { key: 'activeOrder', title: 'Pedido' },
      { key: 'paidOrders', title: 'Pagamentos' }
    ]
  };

  renderOrderFooterInfo() {
    return (
      <View
        style={{
          backgroundColor: '#e6e6e6',
          height: 50,
          flexDirection: 'row'
        }}
      >
        <View style={{ flex: 3 }}>
          <Text style={styles.slideTitle}>
            {this.props.order.totalPrice.toFixed(2)}€
          </Text>
        </View>
        <ScrollView
          style={styles.slidesPay}
          horizontal
          onTouchEnd={() => this.setState({ allowDragging: true })}
          onTouchCancel={() => this.setState({ allowDragging: true })}
          onTouchStart={() => {
            this.setState({ showPaymentOptions: true });
          }}
        >
          <View style={styles.slidePayIcon}>
            <Icon name="payment" />
          </View>
          <Text
            style={Object.assign({}, styles.slideTitle, styles.slidesPayTitle)}
          >
            Pagar
          </Text>
        </ScrollView>
        <Dialog
          visible={this.state.showPaymentOptions}
          onTouchOutside={() => {
            this.setState({ showPaymentOptions: false });
          }}
          rounded
          dialogTitle={
            <DialogTitle
              title="Opções de pagamento"
              style={{
                backgroundColor: '#F7F7F8'
              }}
              hasTitleBar={false}
              align="left"
            />
          }
          footer={
            <DialogFooter>
              <DialogButton
                text="Cancelar"
                bordered
                onPress={() => {
                  this.setState({ showPaymentOptions: false });
                }}
                key="button-1"
              />
            </DialogFooter>
          }
        >
          <DialogContent style={{}}>
            <TouchableHighlight
              onPress={() => {
                this.props.sendOrder();
                this.setState({ showPaymentOptions: false });
              }}
              underlayColor="white"
            >
              <Image
                style={{ height: 90, resizeMode: 'contain' }}
                source={{
                  uri:
                    'https://lh6.ggpht.com/uFiCg8DkG7F2uW4jQSs7PzwZyxuxkyYUEfUAT6ju_qFmvk8WR1z6xG3HKMjWCLm-1rc'
                }}
              />
            </TouchableHighlight>
            <Image
              style={{ height: 100, resizeMode: 'contain' }}
              source={{
                uri:
                  'https://png.pngtree.com/element_our/png_detail/20180723/paypal-logo-icon-png_44634.png'
              }}
            />
          </DialogContent>
        </Dialog>
      </View>
    );
  }

  _renderScene = ({ route }) => {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          style={{ backgroundColor: 'white' }}
          onTouchEnd={() => this.setState({ allowDragging: true })}
          onTouchCancel={() => this.setState({ allowDragging: true })}
          onTouchStart={() => this.setState({ allowDragging: false })}
        >
          {route.key == 'activeOrder' ? <ActiveOrder /> : <PaidOrders />}
        </ScrollView>
        {route.key == 'activeOrder' &&
          this.props.order.totalPrice > 0 &&
          this.renderOrderFooterInfo()}
      </View>
    );
  };

  _renderLabel = ({ route }) => {
    if (route.key == 'paidOrders') {
      return (
        <Animated.Text style={[styles.tabLabel, this.props.labelStyle]}>
          {(route.title + ' (' + this.props.orders.length + ')').toUpperCase()}
        </Animated.Text>
      );
    } else {
      return (
        <Animated.Text style={styles.tabLabel}>
          {route.title.toUpperCase()}
        </Animated.Text>
      );
    }
  };

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
        renderTabBar={props => (
          <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: 'gray', height: 2 }}
            //tabStyle={{backgroundColor: 'white'}}
            //labelStyle={{color: 'black'}}
            renderLabel={this._renderLabel}
            style={{ backgroundColor: 'white' }}
            pressColor="gray"
          />
        )}
      />
    );
  }
}

const styles = {
  tabLabel: {
    backgroundColor: 'transparent',
    color: 'black',
    margin: 8
  },
  slideTitle: {
    fontSize: 21,
    margin: 10
  },
  slidesPay: {
    flex: 1
  },
  slidesPayTitle: {
    textAlign: 'right'
  },
  slidePayIcon: {
    marginTop: 13
  }
};

function mapStateToProps(state) {
  return {
    order: state.order.activeOrder,
    orders: state.order.paidOrders,
    restaurantId: state.restid.restaurantId
  };
}

function mapDispatchToProps(dispatch) {
  return {
    removeProduct: product => dispatch({ type: 'REMOVE_PRODUCT', product }),
    addPaidOrder: order => dispatch({ type: 'ADD_PAID_ORDER', order }),
    resetActiveOrder: () => dispatch({ type: 'RESET_ACTIVE_ORDER' })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTabView);
