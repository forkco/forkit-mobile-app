import React, { Component } from 'react';
import { Keyboard } from 'react-native';
import { StyleSheet, Text, View } from 'react-native';
import { SearchBar, ListItem, Icon } from 'react-native-elements';
import { createFilter } from 'react-native-search-filter';
import HeaderImageScrollView, {
  TriggeringView
} from 'react-native-image-header-scroll-view';
import * as Animatable from 'react-native-animatable';
import { connect } from 'react-redux';

const KEYS_TO_FILTERS = ['name'];
const MIN_HEIGHT = 57;
const MAX_HEIGHT = 200;

class SearchList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      showHeader: true
    };
  }

  searchUpdated(term) {
    this.setState({ searchTerm: term });
  }

  isProductOnOrder(id) {
    for (var i = 0; i < this.props.order.products.length; i++) {
      if (this.props.order.products[i].id == id) return true;
    }
    return false;
  }

  getProductUnits(id) {
    for (var i = 0; i < this.props.order.products.length; i++) {
      if (this.props.order.products[i].id == id)
        return this.props.order.products[i].units;
    }
  }

  renderItems(products) {
    return (
      <View>
        {products.map((p, x) => (
          <View key={x}>
            <ListItem
              title={p.name}
              leftAvatar={{
                //source: { uri: 'https://forkittest.gigalixirapp.com' + p.image }
                source: productsImages[Math.floor(Math.random()*productsImages.length)]
              }}
              titleStyle={{}}
              rightTitle={
                <View style={{ flexDirection: 'row' }}>
                  {!p.available && (
                    <Text
                      style={{
                        color: 'red',
                        paddingRight: 10,
                        fontWeight: 'bold'
                      }}
                    >
                      Indisponível
                    </Text>
                  )}
                  {this.getProductUnits(p.id) && (
                    <Text style={{ fontWeight: 'bold', paddingRight: 5 }}>
                      {this.getProductUnits(p.id)}x
                    </Text>
                  )}
                  <Text style={{}}>{(p.price * 0.01).toFixed(2)}€</Text>
                </View>
              }
              rightIcon={{
                name: 'add-circle',
                onPress: () => {
                  p.available ? this.props.addProduct(p) : {};
                }
              }}
              //containerStyle={{borderColor:'gray', borderTopWidth: 0.5, borderBottomWidth: 0.5}}
              onPress={() => {
                this.props.navigation.navigate('ProductView', { product: p });
              }}
              disabled={!p.available}
              disabledStyle={{ opacity: 0.3 }}
              bottomDivider={true}
            />
          </View>
        ))}
      </View>
    );
  }

  render() {
    const filteredProducts = this.props.list.filter(
      createFilter(this.state.searchTerm, KEYS_TO_FILTERS)
    );
    const headerMaxHeight = this.state.showHeader ? MAX_HEIGHT : MIN_HEIGHT;
    return (
      <View style={{ flex: 1 }}>
        <HeaderImageScrollView
          maxHeight={headerMaxHeight}
          minHeight={MIN_HEIGHT}
          headerImage={
            //uri:
             // 'https://forkittest.gigalixirapp.com' + this.props.categoryImage
             this.props.categoryImage
          }
          renderFixedForeground={() => (
            <View style={{}}>
              <Animatable.View
                style={styles.navTitleView}
                ref={navTitleView => {
                  this.navTitleView = navTitleView;
                }}
              >
                <SearchBar
                  platform="android"
                  cancelIcon={{ type: 'font-awesome', name: 'chevron-left' }}
                  placeholder="Search Product"
                  onChangeText={term => {
                    this.searchUpdated(term);
                    this.state.showHeader
                      ? this.setState({ showHeader: false })
                      : {};
                  }}
                  onClear={() => {
                    this.searchUpdated('');
                  }}
                  containerStyle={{}}
                />
              </Animatable.View>
              <Animatable.View
                style={{ position: 'absolute' }}
                ref={backArrow => {
                  this.backArrow = backArrow;
                }}
              >
                <Icon
                  name="arrow-back"
                  color='white'
                  containerStyle={{ top: 10, left: 8, zIndex: 1 }}
                  size={35}
                  onPress={() => {
                    this.state.showHeader
                      ? this.props.navigation.navigate('ListCategories')
                      : Keyboard.dismiss();
                    this.searchUpdated('');
                    this.setState({ showHeader: true });
                    this.navTitleView.fadeOut(100);
                    this.backArrow.fadeIn(100);
                  }}
                  underlayColor="transparent"
                />
              </Animatable.View>
            </View>
          )}
        >
          <TriggeringView
            onHide={() => {
              if (this.state.showHeader) {
                this.navTitleView.fadeIn(100);
                this.backArrow.fadeOut(100);
              }
            }}
            onDisplay={() => {
              if (this.state.showHeader) {
                this.backArrow.fadeIn(100);
                this.navTitleView.fadeOut(100);
              }
            }}
          >
            {this.state.showHeader && (
              <View style={styles.categoryContainer}>
                <Text style={styles.categoryTitle}>
                  {this.props.categoryName}
                </Text>
              </View>
            )}
          </TriggeringView>
          {this.renderItems(filteredProducts)}
        </HeaderImageScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    order: state.order.activeOrder
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addProduct: product => dispatch({ type: 'ADD_PRODUCT', product }),
    removeProduct: product => dispatch({ type: 'REMOVE_PRODUCT', product })
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchList);

const styles = StyleSheet.create({
  navTitleView: {
    height: MIN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    opacity: 0
  },
  categoryContainer: {
    height: MIN_HEIGHT,
    backgroundColor: 'white',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  },
  categoryTitle: {
    fontSize: 25,
    textAlign: 'center',
    textAlignVertical: 'center',
    //fontFamily: 'monospace',
    fontWeight: 'bold'
  }
});

const productsImages = [
  require('../images/m1.jpg'),
  require('../images/m2.png'),
  require('../images/m3.png'),
  require('../images/m4.png'),
  require('../images/m5.png')
]
