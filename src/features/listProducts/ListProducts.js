import React, { Component } from 'react';
import Swiper from 'react-native-swiper';
import { View } from 'react-native';
import SearchList from './components/SearchList.js';

class ListProducts extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const categories = this.props.navigation.getParam('categories');
    const categoryPosition = this.props.navigation.getParam('category');
    return (
      <Swiper
        showsButtons={false}
        loop={false}
        index={categoryPosition}
        ref={swiper => {
          this.swiper = swiper;
        }}
      >
        {categories.map((u, i) => (
          <View key={i} style={{ flex: 1 }}>
            <SearchList
              categoryName={u.name}
              categoryImage={categoryImages[u.id-1]}
              list={u.products}
              navigation={this.props.navigation}
            />
          </View>
        ))}
      </Swiper>
    );
  }
}

export default ListProducts;

var categoryImages = [
  require('../../features/listCategories/images/category1.jpg'),
  require('../../features/listCategories/images/category2.jpg'),
  require('../../features/listCategories/images/category3.jpg'),
  require('../../features/listCategories/images/category4.png')
]