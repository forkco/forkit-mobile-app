import React, { Component } from 'react';
import { SearchBar, Header, Icon } from 'react-native-elements';
import { Animated, ScrollView, StyleSheet, Keyboard, View } from 'react-native';
import * as Animatable from 'react-native-animatable';

export { ImageScrollView };

export default class ImageScrollView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scrollY: new Animated.Value(0),
      showSearchBar: false
    };
  }

  _renderSearchBar() {
    return (
      <Animatable.View animation="fadeInDown" duration={100}>
        <SearchBar
          autoFocus={true}
          inputStyle={{ top: 5 }}
          platform="android"
          cancelIcon={{
            onPress: () => {
              this.props.searchUpdated('');
              this.setState({ showSearchBar: false });
              Keyboard.dismiss();
            }
          }}
          clearIcon={{
            name: 'close',
            onPress: () => {
              this.props.searchUpdated('');
            }
          }}
          placeholder="Search Restaurant"
          onChangeText={term => {
            this.props.searchUpdated(term);
          }}
        />
      </Animatable.View>
    );
  }

  _renderSearchableView(scrollContent) {
    return (
      <View style={styles.fill}>
        <View style={[styles.header, { height: HEADER_MIN_HEIGHT }]}>
          {this._renderSearchBar()}
        </View>
        <ScrollView style={{ flex: 1, marginTop: HEADER_MIN_HEIGHT }}>
          {scrollContent}
        </ScrollView>
      </View>
    );
  }

  render() {
    const {
      headerImage,
      leftComponent,
      centerComponent,
      rightComponent,
      scrollContent,
      scrollContentStyle,
      logoImage,
      topbarStyle
    } = this.props;

    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [HEADER_MAX_HEIGHT, HEADER_MIN_HEIGHT],
      extrapolate: 'clamp'
    });
    const imageOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [1, 1, 0],
      extrapolate: 'clamp'
    });
    const imageTranslate = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE],
      outputRange: [0, -50],
      extrapolate: 'clamp'
    });
    const textOpacity = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: [0, 0, 1],
      extrapolate: 'clamp'
    });
    /*
    const colorTrans = this.state.scrollY.interpolate({
      inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
      outputRange: ['black', 'black', 'white'],
      extrapolate: 'clamp'
    }); */

    if (this.state.showSearchBar) {
      return this._renderSearchableView(scrollContent);
    }

    return (
      <View style={styles.fill}>
        <ScrollView
          style={styles.fill}
          scrollEventThrottle={16}
          onScroll={Animated.event([
            { nativeEvent: { contentOffset: { y: this.state.scrollY } } }
          ])}
        >
          <View style={[scrollContentStyle, styles.scrollViewContent]}>
            {scrollContent}
          </View>
        </ScrollView>
        <Animated.View style={[styles.header, { height: headerHeight }]}>
          <Animated.Image
            style={[
              styles.backgroundImage,
              {
                opacity: imageOpacity,
                transform: [{ translateY: imageTranslate }]
              }
            ]}
            source={headerImage}
          />
          <View>
            <Header
              containerStyle={[topbarStyle, styles.topbar]}
              leftComponent={
                logoImage ? (
                  <Animated.Image
                    style={{
                      opacity: textOpacity,
                      height: 50,
                      resizeMode: 'contain'
                    }}
                    source={logoImage}
                  />
                ) : (
                  leftComponent
                )
              }
              centerComponent={centerComponent}
              rightComponent={
                rightComponent ? (
                  rightComponent
                ) : (
                  <Icon
                    color="white"
                    style={{ flex: 1 }}
                    name="search"
                    size={30}
                    underlayColor="transparent"
                    onPress={() => {
                      this.setState({ showSearchBar: true });
                    }}
                  />
                )
              }
            />
          </View>
        </Animated.View>
      </View>
    );
  }
}

const HEADER_MAX_HEIGHT = 200;
const HEADER_MIN_HEIGHT = 56;
const HEADER_SCROLL_DISTANCE = HEADER_MAX_HEIGHT - HEADER_MIN_HEIGHT;

//ImageScrollView.propTypes = {
//      headerImage,
//      leftComponent,
//      centerComponent,
//      rightComponent,
//      scrollContent,
//      topbarStyle
//};

const styles = StyleSheet.create({
  fill: {
    flex: 1
  },
  header: {
    position: 'absolute',
    elevation: 3,
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#a83432',
    overflow: 'hidden'
  },
  topbar: {
    paddingTop: 0,
    height: HEADER_MIN_HEIGHT,
    backgroundColor: 'rgba(0, 0, 0, 0)',
    justifyContent: 'space-around',
    borderBottomWidth: 0
  },
  scrollViewContent: {
    marginTop: HEADER_MAX_HEIGHT,
    backgroundColor: '#f4f4f4'
  },
  backgroundImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    width: null,
    height: HEADER_MAX_HEIGHT,
    resizeMode: 'cover'
  },
  row: {
    height: 40,
    margin: 16,
    backgroundColor: '#D3D3D3',
    alignItems: 'center',
    justifyContent: 'center'
  }
});
