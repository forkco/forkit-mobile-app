import React, { Component } from 'react';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { alterOrder, setRestaurantId } from './reducers';
import App from './App.js';

const reducers = combineReducers({
  restid: setRestaurantId,
  order: alterOrder
});

const store = createStore(reducers);

export default class Root extends Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}
