const initialStateOrders = {
  paidOrders: [],
  activeOrder: {
    products: [],
    totalPrice: 0
  }
};

export const setRestaurantId = (state = { restaurantId: 0 }, action) => {
  switch (action.type) {
    case 'SET_RESTAURANT_ID':
      state.restaurantId = action.restaurantId;
      return state;
    default:
      return state;
  }
};

export const alterOrder = (state = initialStateOrders, action) => {
  switch (action.type) {
    case 'ADD_PRODUCT': {
      let totalPrice =
        state.activeOrder.totalPrice + action.product.price * 0.01;
      for (var i = 0; i < state.activeOrder.products.length; i++) {
        if (state.activeOrder.products[i].id == action.product.id) {
          let units = state.activeOrder.products[i].units + 1;
          action.product.units = units;
          return {
            ...state,
            activeOrder: {
              ...state.activeOrder,
              products: [
                ...state.activeOrder.products.map((item, index) => {
                  if (index !== i) {
                    return item;
                  }
                  return {
                    ...item,
                    ...action.product
                  };
                })
              ],
              totalPrice: totalPrice
            }
          };
        }
      }
      action.product.units = 1;
      return {
        ...state,
        activeOrder: {
          ...state.activeOrder,
          products: [...state.activeOrder.products, action.product],
          totalPrice: totalPrice
        }
      };
    }
    case 'REMOVE_PRODUCT':
      for (i = 0; i < state.activeOrder.products.length; i++) {
        if (state.activeOrder.products[i].id == action.product.id) {
          if (state.activeOrder.products[i].units > 1) {
            let units = state.activeOrder.products[i].units - 1;
            let price =
              state.activeOrder.totalPrice - action.product.price * 0.01;
            action.product.units = units;
            return {
              ...state,
              activeOrder: {
                ...state.activeOrder,
                products: [
                  ...state.activeOrder.products.map((item, index) => {
                    if (index !== i) {
                      return item;
                    }
                    return {
                      ...item,
                      ...action.product
                    };
                  })
                ],
                totalPrice: price
              }
            };
          } else {
            break;
          }
        }
      }
      return {
        ...state,
        activeOrder: {
          ...state.activeOrder,
          products: [
            ...state.activeOrder.products.slice(0, i),
            ...state.activeOrder.products.slice(i + 1)
          ],
          totalPrice: state.activeOrder.totalPrice - action.product.price * 0.01
        }
      };
    case 'ADD_PAID_ORDER':
      return {
        ...state,
        paidOrders: [...state.paidOrders, action.order]
      };
    case 'RESET_ACTIVE_ORDER':
      return {
        ...state,
        activeOrder: {
          products: [],
          totalPrice: 0
        }
      };
  }
  return state;
};
