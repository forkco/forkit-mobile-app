import SearchEstablishment from '../features/searchEstablishment/SearchEstablishment.js';
import ListCategories from '../features/listCategories/ListCategories.js';
import ListProducts from '../features/listProducts/ListProducts.js';
import ProductView from '../features/productView/ProductView.js';
import { createStackNavigator } from 'react-navigation';
import { fromLeft, zoomIn } from 'react-navigation-transitions';

const AppNavigator = createStackNavigator(
  {
    SearchEstablishment: {
      screen: SearchEstablishment,
      navigationOptions: { headerStyle: { display: 'none' }, headerLeft: null }
    },
    ListCategories: {
      screen: ListCategories,
      navigationOptions: { headerStyle: { display: 'none' }, headerLeft: null }
    },
    ListProducts: {
      screen: ListProducts,
      navigationOptions: { headerStyle: { display: 'none' }, headerLeft: null }
    },
    ProductView: {
      screen: ProductView,
      navigationOptions: { headerStyle: { display: 'none' }, headerLeft: null }
    }
  },
  {
    initialRouteKey: 'SearchEstablishment',
    transitionConfig: nav => handleCustomTransition(nav)
  }
);

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  // Custom transitions go there
  if (
    prevScene &&
    prevScene.route.routeName === 'SearchEstablishment' &&
    nextScene.route.routeName === 'ListCategories'
  ) {
    return fromLeft();
  } else if (
    prevScene &&
    prevScene.route.routeName === 'ListCategories' &&
    nextScene.route.routeName === 'ListProducts'
  ) {
    return zoomIn();
  }
  return fromLeft();
};

export default AppNavigator;
