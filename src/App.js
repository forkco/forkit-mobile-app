import React, { Component } from 'react';
import OrderSlide from './features/orderSlide/OrderSlide.js';
import { connect } from 'react-redux';
import { createAppContainer } from 'react-navigation';
import AppNavigator from './navigation/index.js';
import { View, Dimensions } from 'react-native';

const { height } = Dimensions.get('window');

const AppContainer = createAppContainer(AppNavigator);

type Props = {};
class App extends Component<Props> {
  constructor(props) {
    super(props);
  }

  render() {
    let styles = {
      container: {
        flex: 1,
        backgroundColor: 'white',
        paddingBottom: height * this.props.reservedSpace
      }
    };
    console.disableYellowBox = true;
    return (
      <View style={styles.container}>
        <AppContainer />
        <OrderSlide />
      </View>
    );
  }
}

function mapStateToProps(store) {
  return {
    reservedSpace:
      store.order.activeOrder.totalPrice > 0 ||
      store.order.paidOrders.length > 0
        ? 0.04
        : 0
  };
}

export default connect(mapStateToProps)(App);
